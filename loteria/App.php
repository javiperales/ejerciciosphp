<?php

    class App
    {

        function __construct()
        {
            session_start();
            $apuesta=[];

        }//constructor

        public function inicio(){
           // echo "<h1>Loteria primitiva</h1>";
            require('vistaTabla.php');

        }//inicio

        public function añadirQuitar(){
            $numero=$_GET['numero'];
            $arrayNumeros=array();

        }//añadir quitar



        public function toggle(){
            $apuesta=$_SESSION["apuesta"];
            $numero=$_GET['numero'];

        foreach ($apuesta as $numeroApostado) {
            if($numeroApostado==$numero){
                unset($apuesta["$numero"]);
                $_SESSION["apuesta"]=$apuesta;
                header("Location:index.php?method=inicio");
                return;
            }
        }
        $apuesta[$numero]=$numero;
        $_SESSION["apuesta"] = $apuesta;
        header("Location:index.php?method=inicio");
        }//toggle

        public function apostar(){
            if(count($_SESSION["apuesta"])<6){
                echo "necesitas 6 numeros para realizar la apuesta";

            }else if(count($_SESSION["apuesta"])==6){
                echo "apuesta simple";
                $_SESSION["valida"]=true;
            }else{
                echo "apuesta multiple";
            }

            $this->comprobarApuesta();

        }//apostar

        public function comprobarApuesta(){
            if(!$_SESSION["valida"]){
                header("Location:index.php?method=inicio");
            }else{
                require("vistaApuesta.php");
            }

        }//comprobar apuesta

        public function reiniciar(){
            unset($_SESSION["apuesta"]);
            unset($_SESSION["valida"]);
            require("vistaTabla.php");
        }

    }//App
